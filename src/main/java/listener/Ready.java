package listener;

//import core.Birthday;
import core.Birthday;
import core.models.Cd;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import util.EMBEDS;
import util.STATICS;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;

import java.time.temporal.Temporal;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Ready extends ListenerAdapter {
    private static final String ANSI_GREEN = "\u001B[32m";
    private static final String ANSI_RESET = "\u001B[0m";


    @Override
    public void onReady(ReadyEvent event){
        initializeBirthdayTask();
        getAdminRoles();
        System.out.println(ANSI_GREEN + "NAIO READY " + ANSI_RESET);



    }



    private static void initializeBirthdayTask(){
        System.out.println("initializing birthday reminder...");
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        long midnight=LocalDateTime.now().until(LocalDate.now().plusDays(1).atStartOfDay().plusMinutes(2), ChronoUnit.MINUTES);
        //ong now = LocalDateTime.now().until(LocalDateTime.now().plusSeconds(20),ChronoUnit.MINUTES);
        scheduler.scheduleAtFixedRate(new Birthday(), midnight, TimeUnit.DAYS.toMinutes(1), TimeUnit.MINUTES);

        System.out.println("initializing birthday reminder done");



    }
    private static void getAdminRoles(){
        Guild g = STATICS.getJDA().getGuilds().get(0);
        List<Role> gRoles = g.getRoles();
        for (Role r : gRoles) {
            if (STATICS.ADMINROLES_STRINGS.contains(r.getName().toLowerCase())) {
                STATICS.ADMINROLES.add(r);
            }
        }
    }


}
