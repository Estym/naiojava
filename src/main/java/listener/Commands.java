package listener;

import core.json.CustomTriggers;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import util.STATICS;
import core.Naio2;
import java.io.IOException;
import java.text.ParseException;

public class Commands extends ListenerAdapter {

    @Override
    public void onMessageReceived(MessageReceivedEvent e){
        if(e.getChannelType() == ChannelType.PRIVATE) return;
        String response = null;
        response = CustomTriggers.sendRespond(e);

        if (response != null && !e.getAuthor().isBot()){
            e.getTextChannel().sendTyping().complete();
            e.getTextChannel().sendMessage(response).complete();
            return;
        }
        if (e.getMessage().getContentRaw().startsWith(STATICS.PREFIX) && !e.getMessage().getAuthor().getId().equals(e.getJDA().getSelfUser().getId())) {
            try {
                Naio2.handleCommand(Naio2.parser.parse(e.getMessage().getContentRaw(),e));
            } catch (ParseException | IOException e1) {
                e1.printStackTrace();
            }
        }else if (e.getMessage().getContentRaw().startsWith(STATICS.getCryptoPrefix()) && !e.getMessage().getAuthor().getId().equals(e.getJDA().getSelfUser().getId()) && e.getTextChannel().getName().toLowerCase().equals("cryptos")) {
            try {
                Naio2.handleCrypto(Naio2.parser.parseCrypto(e.getMessage().getContentRaw(),e));
            } catch (IOException | ParseException e1) {
                e1.printStackTrace();
            }
        }
    }
}
