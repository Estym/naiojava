package util;


import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class CMD_REACTION{
    public static void positive(MessageReceivedEvent event){
        //415684635079606286L
        event.getTextChannel().sendTyping().complete();
        event.getMessage().addReaction("✅").complete();
    }
    public static void negative(MessageReceivedEvent event){
        event.getTextChannel().sendTyping().complete();
        event.getMessage().addReaction("❌").complete();
    }

}
