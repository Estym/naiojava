package util;

import core.models.Cd;
import net.dv8tion.jda.api.entities.Role;

import java.util.ArrayList;
import java.util.List;

public class STATICS{
    public static final String VERSION = "b2.1";
    public static String BOT_TOKEN;
    public static String PREFIX = ".";

    public static net.dv8tion.jda.api.JDA JDA;
    public static String input;
    public static String MUSIC_CHANNEL;
    public static String VOICELOG_CHANNEL;
    public static String CRYPTO_PREFIX = "!";
    public static List<String> ADMINROLES_STRINGS = new ArrayList<>();
    public static List<Role> ADMINROLES = new ArrayList<>();
    public static List<Role> ROLES = new ArrayList<>();

    public static String getVERSION() {
        return VERSION;
    }

    public static String getInput() {
        return input;
    }

    public static void setInput(String input) {
        STATICS.input = input;
    }

    public static List<String> getAdminrolesStrings() {
        return ADMINROLES_STRINGS;
    }

    public static void setAdminrolesStrings(List<String> adminrolesStrings) {
        ADMINROLES_STRINGS = adminrolesStrings;
    }

    public static List<Role> getROLES() {
        return ROLES;
    }

    public static void setROLES(List<Role> ROLES) {
        STATICS.ROLES = ROLES;
    }

    public static List<Role> getADMINROLES() {
        return ADMINROLES;
    }

    public static void setADMINROLES(List<Role> ADMINROLES) {
        STATICS.ADMINROLES = ADMINROLES;
    }

    public static String getCryptoPrefix() {
        return CRYPTO_PREFIX;
    }

    public static void setCryptoPrefix(String cryptoPrefix) {
        CRYPTO_PREFIX = cryptoPrefix;
    }


    public static String getVoicelogChannel(){
        return VOICELOG_CHANNEL;
    }

    public static void setVoicelogChannel(String voicelogChannel){
        VOICELOG_CHANNEL = voicelogChannel;
    }

    public static String getMusicChannel(){
        return MUSIC_CHANNEL;
    }

    public static void setMusicChannel(String musicChannel){
        MUSIC_CHANNEL = musicChannel;
    }

    public static net.dv8tion.jda.api.JDA getJDA(){
        return JDA;
    }

    public static void setJDA(net.dv8tion.jda.api.JDA JDA){
        STATICS.JDA = JDA;
    }

    public static String RAFFLE_CHANNEL ;


    public static String getPREFIX(){
        return PREFIX;
    }

    public static void setPREFIX(String PREFIX){
        STATICS.PREFIX = PREFIX;
    }



    public static String getRaffleChannel(){
        return RAFFLE_CHANNEL;
    }

    public static void setRaffleChannel(String raffleChannel){
        RAFFLE_CHANNEL = raffleChannel;
    }



    public static String getBotToken(){
        return BOT_TOKEN;
    }
    public static void setBotToken(String botToken){
        STATICS.BOT_TOKEN = botToken;
    }
}
