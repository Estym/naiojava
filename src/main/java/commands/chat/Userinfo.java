package commands.chat;

import commands.Command;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import util.CMD_REACTION;

import java.awt.*;
import java.io.IOException;
import java.text.ParseException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Userinfo implements Command{
    @Override
    public boolean called(String[] args, MessageReceivedEvent event){
        return false;
    }

    @Override
    public void action(String[] args, MessageReceivedEvent event) throws ParseException, IOException{
        CMD_REACTION.positive(event);
        event.getTextChannel().sendTyping().queue();
        List<Member> mentionedMembers = event.getMessage().getMentionedMembers();
        if (mentionedMembers.size() >0 && mentionedMembers.size() < 2){
            event.getTextChannel().sendMessage(fetchData(mentionedMembers.get(0),event).build()).complete();

        }else {
            Member member = event.getMember();
            event.getTextChannel().sendMessage(fetchData(member,event).build()).complete();
        }



    }
    private EmbedBuilder fetchData(Member member, MessageReceivedEvent event){
        List<Role> memberRoles = member.getRoles();
        String effectiveName = member.getEffectiveName();
        OffsetDateTime joinDate = member.getTimeJoined();
        String nickname = member.getNickname();
        OnlineStatus onlineStatus = member.getOnlineStatus();

        User user = member.getUser();
        String userName = user.getName();
        String discriminator = user.getDiscriminator();
        String avatarUrl = user.getAvatarUrl();
        OffsetDateTime creationTime = user.getTimeCreated();
        String id = user.getId();

        EmbedBuilder eb = new EmbedBuilder().setTitle("Userinfo for "+ effectiveName).setColor(Color.CYAN).setThumbnail(avatarUrl);

        eb.addField("Nickname", userName,true)
                .addField("Server Name", effectiveName,true);

        eb.addField("Discriminator",discriminator,true)
                .addField("User-ID", id,true);

        eb.addField("Status",onlineStatus.name(),true);
            eb.addBlankField(true);


        eb.addField("Account Created",creationTime.format(DateTimeFormatter.ofPattern("dd MMM YYYY hh:mm")),true)
                .addField("Server joined",joinDate.format(DateTimeFormatter.ofPattern("dd MMM YYYY hh:mm")),true);

        eb.addField("Roles",mapToList(roleMap(memberRoles,event.getGuild())),false);
        return eb;
    }
    private HashMap<Role, Integer> roleMap(List<Role> roles, Guild g){
        HashMap<Role, Integer> map = new HashMap<>();
        for (Role r :roles) {
            map.put(r,g.getMembersWithRoles(r).size());
        }
        return map;
    }

    private String mapToList(HashMap<Role,Integer> roleMap){
        StringBuilder s = new StringBuilder();
        Iterator it = roleMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            Role r = (Role) pair.getKey();
            s.append(r.getAsMention()).append("\n");
            it.remove(); // avoids a ConcurrentModificationException
        }
        return s.toString();
    }
    @Override
    public void executed(boolean success, MessageReceivedEvent event){

    }

    @Override
    public String help(){
        return null;
    }

    @Override
    public String description(){
        return null;
    }

    @Override
    public String commandType(){
        return null;
    }

    @Override
    public int permission(){
        return 0;
    }
}
