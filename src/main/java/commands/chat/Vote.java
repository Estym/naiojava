package commands.chat;

import commands.Command;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageReaction;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;


import java.awt.*;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Vote implements Command{
    @Override
    public boolean called(String[] args, MessageReceivedEvent event){
        return false;
    }
    //question | amount of answers | poss answers...
    @Override
    public void action(String[] args, MessageReceivedEvent event) throws ParseException, IOException{

        switch (args[0]){
            case "start":
                StringBuilder argsAsText = new StringBuilder();
                for (String s :
                        args) {
                    if (!s.equals("start") ){
                        argsAsText.append(s).append(" ");
                    }
                }
                String[] splits = argsAsText.toString().split("\\|");

                String question = splits[0];
                ArrayList<String> possAnswers = new ArrayList<>();

                for (int i = 1 ; i < splits.length; i++){
                    possAnswers.add(splits[i].trim());
                }

                EmbedBuilder embedVoteMessage = new EmbedBuilder().setColor(Color.CYAN).setTitle(question);

                StringBuilder possAnswersStrg = new StringBuilder();

                for (int i = 0 ; i< possAnswers.size(); i++){
                    possAnswersStrg.append(i + 1).append(" - ").append(possAnswers.get(i)).append("\n");
                }

                embedVoteMessage.setDescription(possAnswersStrg.toString());

                Message voteMsg = event.getTextChannel().sendMessage(embedVoteMessage.build()).complete();

                voteMsg.addReaction("\uD83C\uDF4F").complete();


                break;

            case "re":
                event.getMessage().delete().complete();
                List<Message> listRestAction = event.getTextChannel().getHistory().retrievePast(5).complete();

                for (Message m :
                        listRestAction) {
                    System.out.println(m.getContentRaw());
                    MessageReaction messageReaction = m.getReactions().get(0);
                    System.out.println(messageReaction);
                }
        }
    }

    @Override
    public void executed(boolean success, MessageReceivedEvent event){

    }

    @Override
    public String help(){
        return null;
    }

    @Override
    public String description(){
        return null;
    }

    @Override
    public String commandType(){
        return null;
    }

    @Override
    public int permission(){
        return 0;
    }
}
