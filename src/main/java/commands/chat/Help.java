package commands.chat;

import commands.Command;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Region;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import util.CMD_REACTION;
import util.STATICS;

import java.awt.*;
import java.io.IOException;
import java.text.ParseException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Help implements Command{
    @Override
    public boolean called(String[] args, MessageReceivedEvent event){
        return false;
    }

    @Override
    public void action(String[] args, MessageReceivedEvent event) throws ParseException, IOException{
        CMD_REACTION.positive(event);

        EmbedBuilder eb = new EmbedBuilder().setColor(Color.CYAN).setTitle("Help")
                .setDescription("EIGHTs very own bot coded by the GOAT Nick")
                .addField("Commands",STATICS.getPREFIX()+"``raffle - Creates a list from Names and raffles a winner\n"+
                        STATICS.getPREFIX()+"m - Classic Music bot works only in #" + STATICS.getMusicChannel()+"\n"+
                        STATICS.getPREFIX()+"triggers - Add/remove and list all custom\n"+
                        STATICS.getPREFIX()+"bd - Add/remove you birthday from the reminder System\n"+
                        STATICS.getPREFIX()+"clear - Clears x messages from a text-channel.\n"+
                        STATICS.getPREFIX()+"ud - Urban Dictionary\n" +
                        STATICS.getPREFIX()+"cd - creates a countdown [Deputy only]\n" +
                        STATICS.getPREFIX()+"raffle - raffles an amount of people and chooses the winner\n" +
                        STATICS.getPREFIX()+"role - Assign roles to yourself\n" +
                        STATICS.getPREFIX()+"drop - Posts in the box-drop channel\n" +
                        STATICS.getPREFIX()+"userinfo - Displays user related infos``",false);

        Guild g = event.getGuild();
        List<Role> roles = g.getRoles();
        int memberCount = g.getMembers().size();
        String roleList = mapToList(roleMap(roles, g));
        OffsetDateTime creationTime = g.getTimeCreated();
        int vcSize = g.getVoiceChannels().size();
        String splashUrl = g.getIconUrl();
        Region region = g.getRegion();
        int tcSize = g.getTextChannels().size();
        eb.addBlankField(false);
        eb.setThumbnail(splashUrl);
        System.out.println(splashUrl);
        eb.addField("Total Members",String.valueOf(memberCount),true);
        eb.addField("Total Textchannel",String.valueOf(tcSize),true);
        eb.addField("Total Voicechannel",String.valueOf(vcSize),true);
        eb.addField("Creation Date",""+creationTime.format(DateTimeFormatter.ofPattern("dd MMM YYYY hh:mm")),true);
        eb.addField("Region",region.getName()+" "+region.getEmoji(),true);
        eb.addField("Roles",roleList,false);
        event.getTextChannel().sendMessage(eb.build()).complete();

    }

    private HashMap<Role, Integer> roleMap(List<Role> roles, Guild g){
        HashMap<Role, Integer> map = new HashMap<>();
        for (Role r :roles) {
            map.put(r,g.getMembersWithRoles(r).size());
        }
        return map;
    }

    private String mapToList(HashMap<Role,Integer> roleMap){
        StringBuilder s = new StringBuilder();
        Iterator it = roleMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            Role r = (Role) pair.getKey();
            int count = (int) pair.getValue();
            s.append(r.getAsMention()).append("[").append(count).append("]").append("\n");
            it.remove(); // avoids a ConcurrentModificationException
        }
        return s.toString();
    }

    @Override
    public void executed(boolean success, MessageReceivedEvent event){

    }

    @Override
    public String help(){
        return null;
    }

    @Override
    public String description(){
        return null;
    }

    @Override
    public String commandType(){
        return null;
    }

    @Override
    public int permission(){
        return 0;
    }
}
