package commands.chat;

import commands.Command;
import core.json.CustomTriggers;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import util.EMBEDS;
import util.CMD_REACTION;

import java.io.IOException;
import java.text.ParseException;

public class Triggers implements Command{
    @Override
    public boolean called(String[] args, MessageReceivedEvent event){
        return false;
    }

    @Override
    public void action(String[] args, MessageReceivedEvent event) throws ParseException, IOException{
        event.getTextChannel().sendTyping().complete();
        if (args.length < 1){
            event.getTextChannel().sendMessage(EMBEDS.helpEmbed(help(),description()).build()).complete();
            CMD_REACTION.negative(event);
            return;
        }

        String identifier = args[0] ;
        StringBuilder allArgs = new StringBuilder();
        for(int i = 1 ; i < args.length ; i++){
            allArgs.append(args[i]).append(" ");
        }

        switch (identifier){
            case "add":
                String[] split = allArgs.toString().split("\\|");
                String trigger = split[0].trim();
                String response = split[1].trim();

                try {
                    CustomTriggers.addTrigger(trigger, response);
                    CMD_REACTION.positive(event);
                } catch (org.json.simple.parser.ParseException e) {
                    e.printStackTrace();
                    CMD_REACTION.negative(event);

                }

            case "all":
                PrivateChannel privateChannel = event.getMember().getUser().openPrivateChannel().complete();

                try {
                    String allTriggers = CustomTriggers.getAllTriggers();
                    CMD_REACTION.positive(event);
                    privateChannel.sendMessage(allTriggers).complete();
                } catch (org.json.simple.parser.ParseException e) {
                    e.printStackTrace();
                    CMD_REACTION.negative(event);
                }

                return;
            case "remove":
                try {
                    CustomTriggers.delteTrigger(allArgs.toString());
                    CMD_REACTION.positive(event);

                } catch (org.json.simple.parser.ParseException e) {
                    e.printStackTrace();
                    CMD_REACTION.negative(event);

                }
        }

    }

    @Override
    public void executed(boolean success, MessageReceivedEvent event){
        System.out.println("Executed Triggers Command " + !success + " from " +event.getMember().getUser().getName()+"#"+event.getMember().getUser().getDiscriminator());

    }

    @Override
    public String help(){
        return  "**.t / .triggers add** *some trigger* | *some response*\n" +
                "**.t / .triggers remove** *some trigger*\n"+
                "**.t/ .triggers all**";
    }

    @Override
    public String description(){
        return "Manages the custom responses. You can add, remove and get a list of all triggers.";
    }

    @Override
    public String commandType(){
        return null;
    }

    @Override
    public int permission(){
        return 0;
    }
}
