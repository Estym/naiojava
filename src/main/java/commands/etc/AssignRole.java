package commands.etc;

import commands.Command;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import util.CMD_REACTION;
import util.EMBEDS;
import util.Power;
import util.STATICS;

import java.awt.*;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Objects;

import static util.EMBEDS.nopowerEmbed;

public class AssignRole implements Command{
    @Override
    public boolean called(String[] args, MessageReceivedEvent event){
        return false;
    }

    @Override
    public void action(String[] args, MessageReceivedEvent event) throws ParseException, IOException{

        String roleName = null;
        Role role = null;
        if (args.length < 1){
            CMD_REACTION.negative(event);
            event.getTextChannel().sendMessage(EMBEDS.helpEmbed(help(),description()).build()).complete();
            return;
        }

        if (args.length > 1) {
            roleName = args[1];
            role = event.getGuild().getRolesByName(roleName, false).get(0);
        }

        switch (args[0]) {
            case "add":
                if (STATICS.ROLES.contains(role)) {
                    CMD_REACTION.positive(event);
                    Objects.requireNonNull(event.getMember()).getRoles().add(role);
                } else {
                    CMD_REACTION.negative(event);
                }

                break;
            case "remove":
                CMD_REACTION.positive(event);
                Objects.requireNonNull(event.getMember()).getRoles().remove(role);
                break;

            case "enable":

                    if (Power.noPower(event.getMember())) {
                        CMD_REACTION.negative(event);
                        event.getTextChannel().sendMessage(nopowerEmbed().build()).complete();
                        return;
                    }

                    CMD_REACTION.positive(event);
                    STATICS.ROLES.add(role);

                break;

            case "disable":

                    if (Power.noPower(event.getMember())) {
                        CMD_REACTION.negative(event);
                        event.getTextChannel().sendMessage(nopowerEmbed().build()).complete();
                        return;
                    }

                    CMD_REACTION.positive(event);
                    STATICS.ROLES.remove(role);

                break;

            case "list":
                CMD_REACTION.positive(event);

                    StringBuilder mentionedRoles = new StringBuilder();
                    for (Role s :
                            STATICS.ROLES) {
                        mentionedRoles.append(s.getAsMention()).append("\n");
                    }

                    EmbedBuilder builder = new EmbedBuilder()
                            .setColor(Color.LIGHT_GRAY)
                            .setDescription("Following roles can be assigned.\n\n" + mentionedRoles.toString());
                    event.getTextChannel().sendMessage(builder.build()).complete();


                break;

        }
    }

    @Override
    public void executed(boolean success, MessageReceivedEvent event){

    }

    @Override
    public String help(){
        return "**.role add** Rolename\n" +
                "**.role remove** Rolename\n" +
                "**.role list**\n" +
                "**__Admin Only__**\n" +
                "**.role enable** Rolename\n" +
                "**.role disable** Rolename\n"
                ;
    }

    @Override
    public String description(){
        return "Adds or removes a role from an user. Admins can enable and disable roles.";
    }

    @Override
    public String commandType(){
        return null;
    }

    @Override
    public int permission(){
        return 0;
    }
}
