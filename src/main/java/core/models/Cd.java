package core.models;

import java.time.LocalDateTime;
import java.util.Timer;

public class Cd{
    private String title;
    private LocalDateTime startDate;
    private String CAT_ID;
    private Timer timer;

    public Timer getTimer(){
        return timer;
    }

    public void setTimer(Timer timer){
        this.timer = timer;
    }

    public String getCAT_ID(){
        return CAT_ID;
    }

    public void setCAT_ID(String CAT_ID){
        this.CAT_ID = CAT_ID;
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public LocalDateTime getStartDate(){
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate){
        this.startDate = startDate;
    }
}
