package core.json;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import util.STATICS;

import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Config {
    private static JSONParser JSONPARSER = new JSONParser();

    public static void setUpStatics(){
        try {
            FileReader reader = new FileReader("config.json");
            JSONObject obj = (JSONObject) JSONPARSER.parse(reader);

            STATICS.setBotToken(obj.get("token").toString());
            STATICS.setPREFIX(obj.get("prefix").toString());
            STATICS.setCryptoPrefix(obj.get("crypto_prefix").toString());
            STATICS.setMusicChannel(obj.get("music_channel").toString());
            STATICS.setRaffleChannel(obj.get("raffle_channel").toString());
            STATICS.setVoicelogChannel(obj.get("voice_log").toString());
            //STATICS.setAdminrolesStrings(obj.get("power_roles"));

            System.out.println(obj.get("power_roles"));
           JSONArray array = (JSONArray) obj.get("power_roles");
            List<Object> objects = Arrays.asList(array.toArray());
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }


    }

}
