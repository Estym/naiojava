package core;

import commands.Command;
import commands.chat.Clear;
import commands.chat.Triggers;
import commands.chat.Vote;
import commands.etc.AssignRole;
import commands.etc.Covid;
import commands.music.Music;
import commands.sro.Drop;
import commands.sro.Raffle;
import core.crypto.Crypto;
import core.json.Config;
import listener.*;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import util.STATICS;

import javax.security.auth.login.LoginException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.HashMap;

public class Naio2 {
    private static JDABuilder jdaBuilder;
    public static final CommandParser parser = new CommandParser();

    public static HashMap<String, Command> commands = new HashMap<>();

    public static void main(String[] args) {
        try {
            System.out.println("initializing bot...");
            setupBot();
            System.out.println("initializing bot done");

        } catch (LoginException e) {
            e.printStackTrace();
        }
    }

    private static void setupBot() throws LoginException {
        System.out.println("initializing jda...");
        loadConfig();
        jdaBuilder = JDABuilder.createDefault(STATICS.getBotToken())
                .setActivity(Activity.streaming(STATICS.getPREFIX()+"help | "+STATICS.VERSION,
                        "https://www.twitch.tv/justFame01"))
                .setAutoReconnect(true)
                .setBulkDeleteSplittingEnabled(true);

        initializeListeners();
        initializeCommands();
        JDA build = jdaBuilder.build();
        STATICS.setJDA(build);
        System.out.println("initializing jda done");

    }

    private static void loadConfig(){
        System.out.println("loading Config");
        Config.setUpStatics();
        System.out.println("loading config complete");
    }
    private static void initializeListeners() {
        System.out.println("initializing listeners...");
        jdaBuilder.addEventListeners(new Ready(),new Leave(), new Commands(), new Stream(), new Voice(), new Nick());
        System.out.println("initializing listeners done");

    }

    private static void initializeCommands() {
        System.out.println("initializing commands...");

        commands.put("raffle", new Raffle());
        commands.put("r", new Raffle());
        commands.put("triggers", new Triggers());
        commands.put("t", new Triggers());
       // commands.put("s", new commands.admin.Settings());
        commands.put("bd", new commands.etc.Birthday());
        commands.put("m", new Music());
        commands.put("clear", new Clear());
        commands.put("c", new Clear());
        commands.put("cry", new commands.etc.Crypto());
        commands.put("crypto", new commands.etc.Crypto());
        commands.put("ud", new commands.chat.Urban());
        commands.put("h", new commands.chat.Help());
        commands.put("help", new commands.chat.Help());
        commands.put("ui", new commands.chat.Userinfo());
        commands.put("userinfo", new commands.chat.Userinfo());
        commands.put("role", new AssignRole());
        commands.put("drop", new Drop());
        commands.put("r2", new Raffle.Raffle2());
        commands.put("v", new Vote());
        commands.put("covid", new Covid());
        System.out.println("initializing commands done.");

    }

    public static void handleCommand(CommandParser.CommandContainer cmd) throws ParseException, IOException {

        if (commands.containsKey(cmd.invoke.toLowerCase())) {

            boolean safe = commands.get(cmd.invoke.toLowerCase()).called(cmd.args, cmd.event);

            if (!safe) {
                commands.get(cmd.invoke.toLowerCase()).action(cmd.args, cmd.event);
                commands.get(cmd.invoke.toLowerCase()).executed(safe, cmd.event);
            } else {
                commands.get(cmd.invoke.toLowerCase()).executed(safe, cmd.event);
            }

        }
    }



    public static void handleCrypto(CommandParser.CommandContainer cmd) throws IOException, ParseException{

        if (cmd.args.length <1){
            Crypto.getCurrency(cmd.invoke, cmd.event);
        }else {
            if (commands.containsKey(cmd.invoke.toLowerCase())){
                commands.get(cmd.invoke.toLowerCase()).action(cmd.args, cmd.event);
            }
        }

    }

}
